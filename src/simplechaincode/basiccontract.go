/*******************************************************************************
 * Copyright (c) 2021 Belltane, LLC
 *
 * All rights reserved.
 *
 * Contributors:
 *   James Worthington - Initial implementation
 *
 * Version: 0.10
 *
 *******************************************************************************/

package main

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/golang/protobuf/ptypes"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const contractTag = "Basic:"

// BasicChaincode implements the fabric-contract-api-go programming model
type BasicChaincode struct {
	contractapi.Contract
}

// CreateBasicContract initializes a new contract in the ledger
func (t *BasicChaincode) CreateBasicContract(ctx contractapi.TransactionContextInterface, args string) error {

	fmt.Println(contractTag, "CreateBasicContract entered ")
	basicContractEventsArray := []BasicContract{}
	basicContract := BasicContract{}

	err := json.Unmarshal([]byte(args), &basicContract)
	if err != nil {
		fmt.Println(contractTag, "Create Contract: error while unmarshaling contract %s", err)
		return fmt.Errorf("failed to unmarshal basicContract: %v", err)
	}

	basicContract.DocType = docPrefixBasic
	key := docPrefixBasic + strconv.Itoa(basicContract.ContractId)

	exists, err := t.ContractExists(ctx, key)
	if err != nil {
		return fmt.Errorf("failed to get basicContract: %v", err)
	}
	if exists {
		fmt.Println(contractTag, "Create Contract: Contract Already Exists %s", err)
		// TODO Enable line below to enforce uniqueness
		// return fmt.Errorf("basicContract already exists: %s", key)
	}

	basicContractBytes, err := json.Marshal(basicContract)
	if err != nil {
		return err
	}
	basicContractEventsArray = append(basicContractEventsArray, basicContract)
	eventName := "CreateContract"
	basicContractEventsBytes, err := json.Marshal(basicContractEventsArray)
	if err != nil {
		fmt.Println(contractTag, "Create Contract Event: error while marshaling contractEventBytes %s", err)
		// TODO Add to Result for error
		// result := Result{}
		// result.Record = "Writing Events Error BatchID: " + strconv.Itoa(batchID)
		// result.Value = "Create Claim: error while marshaling claimsEventsBytes " + err.Error()
		// results.ResultType = "Failure"
		// results.Results = append(results.Results, result)
	}
	ctx.GetStub().SetEvent(eventName, basicContractEventsBytes)
	// TODO Catch error

	return ctx.GetStub().PutState(docPrefixBasic+strconv.Itoa(basicContract.ContractId), basicContractBytes)
	// TODO Change to catch error THEN write the event and return result.

}

// GetBasicContractByID retrieves a basic contract from the ledger
func (t *BasicChaincode) GetBasicContractByID(ctx contractapi.TransactionContextInterface, basicContractID string) (*BasicContract, error) {
	fmt.Println(contractTag, "Finding Basic Contract:", basicContractID)
	basicContractBytes, err := ctx.GetStub().GetState(basicContractID)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Basic Contract %s: %v", basicContractID, err)
	}
	if basicContractBytes == nil {
		return nil, fmt.Errorf("Basic Contract %s does not exist", basicContractID)
	}

	var basicContract *BasicContract
	err = json.Unmarshal(basicContractBytes, &basicContract)
	if err != nil {
		return nil, err
	}

	return basicContract, nil
}

// DeleteBasicContractByID removes a basic contract key-value pair from the ledger
func (t *BasicChaincode) DeleteBasicContractByID(ctx contractapi.TransactionContextInterface, basicContractID string) error {
	// basicContract, err := t.GetBasicContractByID(ctx, basicContractID)

	// Delete
	return ctx.GetStub().DelState(basicContractID)
}

// GetBasicContractHistoryByID returns the chain of custody for a contract since creation.
func (t *BasicChaincode) GetBasicContractHistoryByID(ctx contractapi.TransactionContextInterface, basicContractID string) ([]GetBasicContractsResultsPaged, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(basicContractID)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var records []GetBasicContractsResultsPaged
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		var basicContract *BasicContract
		err = json.Unmarshal(response.Value, &basicContract)
		if err != nil {
			return nil, err
		}

		timestamp, err := ptypes.Timestamp(response.Timestamp)
		if err != nil {
			return nil, err
		}
		record := GetBasicContractsResultsPaged{
			TxId:      response.TxId,
			Timestamp: timestamp,
			Record:    basicContract,
		}
		records = append(records, record)
	}

	return records, nil
}

// ContractExists returns true when contract with given ID exists in the ledger.
func (t *BasicChaincode) ContractExists(ctx contractapi.TransactionContextInterface, basicContractID string) (bool, error) {
	basicContractBytes, err := ctx.GetStub().GetState(basicContractID)
	if err != nil {
		return false, fmt.Errorf("failed to read contract %s from world state. %v", basicContractID, err)
	}

	return basicContractBytes != nil, nil
}

// constructQueryResponseFromIterator constructs a slice of basic contracts from the resultsIterator
func constructQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) ([]*BasicContract, error) {
	var basicContracts []*BasicContract
	for resultsIterator.HasNext() {
		queryResult, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		var basicContract *BasicContract
		err = json.Unmarshal(queryResult.Value, &basicContract)
		if err != nil {
			return nil, err
		}
		basicContracts = append(basicContracts, basicContract)
	}

	return basicContracts, nil
}

// getQueryResultForQueryString executes the passed in query string.
func getQueryResultForQueryString(ctx contractapi.TransactionContextInterface, queryString string) ([]*BasicContract, error) {
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	return constructQueryResponseFromIterator(resultsIterator)
}

// getQueryResultForQueryStringWithPagination executes the passed in query string with pagination details
func getQueryResultForQueryStringWithPagination(
	ctx contractapi.TransactionContextInterface,
	queryString string,
	pageSize int32,
	bookmark string) ([]*BasicContract, error) {

	resultsIterator, _, err := ctx.GetStub().GetQueryResultWithPagination(queryString, pageSize, bookmark)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	return constructQueryResponseFromIterator(resultsIterator)
}

// InitLedger creates the initial set of assets in the ledger.
func (t *BasicChaincode) InitLedger(ctx contractapi.TransactionContextInterface) error {
	fmt.Printf("Contracts - InitLedger")
	/*
		 basicContract := []BasicContract{
			 {DocType: "asset", ID: "asset1", Color: "blue", Size: 5, Owner: "Tomoko", AppraisedValue: 300},
			 {DocType: "asset", ID: "asset2", Color: "red", Size: 5, Owner: "Brad", AppraisedValue: 400},
			 {DocType: "asset", ID: "asset3", Color: "green", Size: 10, Owner: "Jin Soo", AppraisedValue: 500},
			 {DocType: "asset", ID: "asset4", Color: "yellow", Size: 10, Owner: "Max", AppraisedValue: 600},
			 {DocType: "asset", ID: "asset5", Color: "black", Size: 15, Owner: "Adriana", AppraisedValue: 700},
			 {DocType: "asset", ID: "asset6", Color: "white", Size: 15, Owner: "Michel", AppraisedValue: 800},
		 }

		 for _, basicContract := range basicContracts {
			 basicContractBytes, err := json.Marshal(basicContract)
			 if err != nil {
				 return err
			 }

			 err = ctx.GetStub().PutState(asset.ID, basicContractBytes)
			 if err != nil {
				 return fmt.Errorf("failed to put to world state. %v", err)
			 }
		 }
	*/

	return nil
}
