/*******************************************************************************
 * Copyright (c) 2021 Belltane, LLC
 *
 * All rights reserved.
 *
 * Contributors:
 *   James Worthington - Initial implementation
 *
 * Version: 0.10
 *
 *******************************************************************************/

package main

import "time"

// BasicContract Schema

type BasicContract struct {
	DocType             string `json:"docType"`
	ContractId          int    `json:"contractId"`
	ContractName        string `json:"contractName"`
	ContractDescription string `json:"contractDescription"`
}

// GetBasicContractsResultsPaged structure used for handling result of a paged Rebate Contract query
type GetBasicContractsResultsPaged struct {
	Record              *BasicContract
	TxId                string    `json:"txID"`
	Timestamp           time.Time `json:"timestamp"`
	FetchedRecordsCount int       `json:"fetchedRecordsCount"`
	Bookmark            string    `json:"bookmark"`
}
