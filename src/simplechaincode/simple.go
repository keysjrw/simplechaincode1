/*******************************************************************************
 * Copyright (c) 2021 Belltane, LLC
 *
 * All rights reserved.
 *
 * Contributors:
 *   James Worthington - Initial implementation
 *
 * Version: 0.10
 *
 *******************************************************************************/

package main

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const docPrefixBasic = "Basic"

//type ServerConfig struct {
//	CCID    string
//	Address string
//

func main() {
	log.Printf("Entering Simple Main")
	//basicChaincode, err := contractapi.NewChaincode(&BasicChaincode{})

	key, err := ioutil.ReadFile(os.Getenv("TLS_KEY_FILE"))
	if err != nil {
		//logger.Errorf("Cannot read key file: %s", err)
	}

	cert, err := ioutil.ReadFile(os.Getenv("TLS_CERT_FILE"))
	if err != nil {
		//logger.Errorf("Cannot read cert file: %s", err)
	}

	ca, err := ioutil.ReadFile(os.Getenv("TLS_ROOTCERT_FILE"))
	if err != nil {
		//logger.Errorf("Cannot read ca cert file: %s", err)
	}

	chaincode, err := contractapi.NewChaincode(new(BasicChaincode))

	server := &shim.ChaincodeServer{
		CCID:    os.Getenv("CHAINCODE_CCID"),
		Address: os.Getenv("CHAINCODE_ADDRESS"),
		CC:      chaincode,
		TLSProps: shim.TLSProperties{
			Disabled:      false,
			Key:           key,
			Cert:          cert,
			ClientCACerts: ca,
		},
	}

	// basicChaincode.getStub()
	log.Printf("Main: Starting Basic Chaincode")
	if err := server.Start(); err != nil {
		log.Panicf("Error starting basicChaincode: %v", err)
	}
}
